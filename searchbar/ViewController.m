//
//  ViewController.m
//  searchbar
//
//  Created by CLI112 on 10/6/15.
//  Copyright (c) 2015 CLI112. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (){
    NSArray *searchresult;
    NSArray *celldata;

}



@property (strong, nonatomic) IBOutlet UITableView *tebleview;
@property (strong, nonatomic) IBOutlet UISearchBar *searchbar;


@end

@implementation ViewController
@synthesize tebleview;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    celldata=@[@"india",@"pakistan",@"nepal",@"italy",@"canada",@"indoneshia",@"france",@"afganistan",@"maleshia"];
    searchresult=celldata;
    [tebleview reloadData];
    
    // Do any additional setup after loading the view, typically from a nib.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return  searchresult.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   UITableViewCell*cell=[tableView dequeueReusableCellWithIdentifier:@"sell"];
    cell.textLabel.text=searchresult[indexPath.row];
    return  cell;
   
}

    - (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSPredicate *resultpredicate=[NSPredicate predicateWithFormat:@"self contains[c] %@",searchText];
    NSArray *result=[celldata filteredArrayUsingPredicate:resultpredicate];
    searchresult=[NSArray arrayWithArray:result];
    [tebleview reloadData];
    
    if (searchresult.count==0)
    {
        searchresult=[NSArray arrayWithArray:celldata];
    }
    [tebleview reloadData];
}
    
    


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
