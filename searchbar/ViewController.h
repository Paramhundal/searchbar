//
//  ViewController.h
//  searchbar
//
//  Created by CLI112 on 10/6/15.
//  Copyright (c) 2015 CLI112. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>


@end

